-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: caso3
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `asignatura`
--

DROP TABLE IF EXISTS `asignatura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignatura` (
  `IdAsignatura` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `IdEstado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdAsignatura`),
  KEY `EstadoAsignatura` (`IdEstado`),
  CONSTRAINT `asignatura_ibfk_1` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignatura`
--

LOCK TABLES `asignatura` WRITE;
/*!40000 ALTER TABLE `asignatura` DISABLE KEYS */;
INSERT INTO `asignatura` VALUES (1,'Matematicas',NULL),(2,'Etica',NULL),(3,'Informatica',NULL),(4,'Artes',NULL),(5,'Filosofia',NULL),(6,'Sociales',NULL),(7,'Fisica',NULL),(8,'Educacion Fisica',NULL),(9,'Español',NULL),(10,'Ingles',NULL),(11,'Matematicas',NULL),(12,'Etica',NULL),(13,'Informatica',NULL),(14,'Artes',NULL),(15,'Filosofia',NULL),(16,'Sociales',NULL),(17,'Fisica',NULL),(18,'Educacion Fisica',NULL),(19,'Español',NULL),(20,'Ingles',NULL),(21,'Matematicas',NULL),(22,'Etica',NULL),(23,'Informatica',NULL),(24,'Artes',NULL),(25,'Filosofia',NULL),(26,'Sociales',NULL),(27,'Fisica',NULL),(28,'Educacion Fisica',NULL),(29,'Español',NULL),(30,'Ingles',NULL),(31,'Matematicas',NULL),(32,'Etica',NULL),(33,'Informatica',NULL),(34,'Artes',NULL),(35,'Filosofia',NULL),(36,'Sociales',NULL),(37,'Fisica',NULL),(38,'Educacion Fisica',NULL),(39,'Español',NULL),(40,'Ingles',NULL),(41,'Matematicas',NULL),(42,'Etica',NULL),(43,'Informatica',NULL),(44,'Artes',NULL),(45,'Filosofia',NULL),(46,'Sociales',NULL),(47,'Fisica',NULL),(48,'Educacion Fisica',NULL),(49,'Español',NULL),(50,'Ingles',NULL);
/*!40000 ALTER TABLE `asignatura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asistencia`
--

DROP TABLE IF EXISTS `asistencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asistencia` (
  `IdAsistencia` bigint(20) NOT NULL AUTO_INCREMENT,
  `Asistencias` bigint(20) DEFAULT NULL,
  `Asignatura` varchar(100) DEFAULT NULL,
  `NombreAlumno` varchar(100) DEFAULT NULL,
  `IdEstado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdAsistencia`),
  KEY `EstadoAsistencia` (`IdEstado`),
  CONSTRAINT `asistencia_ibfk_1` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asistencia`
--

LOCK TABLES `asistencia` WRITE;
/*!40000 ALTER TABLE `asistencia` DISABLE KEYS */;
INSERT INTO `asistencia` VALUES (1,1,'Matematicas','Brayan Rojas',NULL),(2,2,'Fisica','Julian Maldonado',NULL),(3,3,'Etica','Brayan olivares',NULL),(4,4,'Sociales','Jason Rodriguez',NULL),(5,5,'Filosofia','Andrea Torres',NULL),(6,6,'Español','Dayana Rojas',NULL),(7,7,'Ingles','Esperanza torres',NULL),(8,8,'Informatica','Luis Lopez',NULL),(9,9,'Educacion Fisica','Felipe cortes',NULL),(10,10,'Artes','Maria Ruiz',NULL),(11,1,'Matematicas','Brayan Rojas',NULL),(12,2,'Fisica','Julian Maldonado',NULL),(13,3,'Etica','Brayan olivares',NULL),(14,4,'Sociales','Jason Rodriguez',NULL),(15,5,'Filosofia','Andrea Torres',NULL),(16,6,'Español','Dayana Rojas',NULL),(17,7,'Ingles','Esperanza torres',NULL),(18,8,'Informatica','Luis Lopez',NULL),(19,9,'Educacion Fisica','Felipe cortes',NULL),(20,10,'Artes','Maria Ruiz',NULL),(21,1,'Matematicas','Brayan Rojas',NULL),(22,2,'Fisica','Julian Maldonado',NULL),(23,3,'Etica','Brayan olivares',NULL),(24,4,'Sociales','Jason Rodriguez',NULL),(25,5,'Filosofia','Andrea Torres',NULL),(26,6,'Español','Dayana Rojas',NULL),(27,7,'Ingles','Esperanza torres',NULL),(28,8,'Informatica','Luis Lopez',NULL),(29,9,'Educacion Fisica','Felipe cortes',NULL),(30,10,'Artes','Maria Ruiz',NULL),(31,1,'Matematicas','Brayan Rojas',NULL),(32,2,'Fisica','Julian Maldonado',NULL),(33,3,'Etica','Brayan olivares',NULL),(34,4,'Sociales','Jason Rodriguez',NULL),(35,5,'Filosofia','Andrea Torres',NULL),(36,6,'Español','Dayana Rojas',NULL),(37,7,'Ingles','Esperanza torres',NULL),(38,8,'Informatica','Luis Lopez',NULL),(39,9,'Educacion Fisica','Felipe cortes',NULL),(40,10,'Artes','Maria Ruiz',NULL),(41,1,'Matematicas','Brayan Rojas',NULL),(42,2,'Fisica','Julian Maldonado',NULL),(43,3,'Etica','Brayan olivares',NULL),(44,4,'Sociales','Jason Rodriguez',NULL),(45,5,'Filosofia','Andrea Torres',NULL),(46,6,'Español','Dayana Rojas',NULL),(47,7,'Ingles','Esperanza torres',NULL),(48,8,'Informatica','Luis Lopez',NULL),(49,9,'Educacion Fisica','Felipe cortes',NULL),(50,10,'Artes','Maria Ruiz',NULL);
/*!40000 ALTER TABLE `asistencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calificacion`
--

DROP TABLE IF EXISTS `calificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calificacion` (
  `IdCalificacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreAlumno` varchar(100) DEFAULT NULL,
  `IdEstado` bigint(20) DEFAULT NULL,
  `nota` double DEFAULT NULL,
  PRIMARY KEY (`IdCalificacion`),
  KEY `EstadoCalificacion` (`IdEstado`),
  CONSTRAINT `calificacion_ibfk_1` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calificacion`
--

LOCK TABLES `calificacion` WRITE;
/*!40000 ALTER TABLE `calificacion` DISABLE KEYS */;
INSERT INTO `calificacion` VALUES (1,'Brayan Rojas',NULL,2),(2,'Felipe Olivares',NULL,3),(3,'Lizeth Pardo',NULL,4),(4,'Leidy Granados',NULL,5),(5,'Ginna Castañeda',NULL,3),(6,'Yeison Poblador',NULL,3),(7,'Andrea Torres',NULL,2),(8,'Yurany Martines',NULL,5),(9,'Monica Samanta',NULL,4),(10,'Luis Lopez',NULL,3),(11,'Brayan Rojas',NULL,2),(12,'Felipe Olivares',NULL,3),(13,'Lizeth Pardo',NULL,4),(14,'Leidy Granados',NULL,5),(15,'Ginna Castañeda',NULL,3),(16,'Yeison Poblador',NULL,3),(17,'Andrea Torres',NULL,2),(18,'Yurany Martines',NULL,5),(19,'Monica Samanta',NULL,4),(20,'Luis Lopez',NULL,3),(21,'Brayan Rojas',NULL,2),(22,'Felipe Olivares',NULL,3),(23,'Lizeth Pardo',NULL,4),(24,'Leidy Granados',NULL,5),(25,'Ginna Castañeda',NULL,3),(26,'Yeison Poblador',NULL,3),(27,'Andrea Torres',NULL,2),(28,'Yurany Martines',NULL,5),(29,'Monica Samanta',NULL,4),(30,'Luis Lopez',NULL,3),(31,'Brayan Rojas',NULL,2),(32,'Felipe Olivares',NULL,3),(33,'Lizeth Pardo',NULL,4),(34,'Leidy Granados',NULL,5),(35,'Ginna Castañeda',NULL,3),(36,'Yeison Poblador',NULL,3),(37,'Andrea Torres',NULL,2),(38,'Yurany Martines',NULL,5),(39,'Monica Samanta',NULL,4),(40,'Luis Lopez',NULL,3),(41,'Brayan Rojas',NULL,2),(42,'Felipe Olivares',NULL,3),(43,'Lizeth Pardo',NULL,4),(44,'Leidy Granados',NULL,5),(45,'Ginna Castañeda',NULL,3),(46,'Yeison Poblador',NULL,3),(47,'Andrea Torres',NULL,2),(48,'Yurany Martines',NULL,5),(49,'Monica Samanta',NULL,4),(50,'Luis Lopez',NULL,3);
/*!40000 ALTER TABLE `calificacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curso` (
  `IdCurso` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreAlumnos` varchar(100) DEFAULT NULL,
  `Cantidad` bigint(20) DEFAULT NULL,
  `IdEstado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdCurso`),
  KEY `EstadoCurso` (`IdEstado`),
  CONSTRAINT `curso_ibfk_1` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso`
--

LOCK TABLES `curso` WRITE;
/*!40000 ALTER TABLE `curso` DISABLE KEYS */;
INSERT INTO `curso` VALUES (2,'Brayan Rojas',35,NULL),(3,'Andrea Ramirez',33,NULL),(4,'Lizeth Pardo',23,NULL),(5,'Brayan Olivares',30,NULL),(6,'Yeison Trujillo',32,NULL),(7,'Jason Rodriguez',31,NULL),(8,'Gladys Torres',30,NULL),(9,'Liz Ramirez',32,NULL),(10,'Dayana Correa',33,NULL),(11,'Julieth Nieto',35,NULL),(12,'Brayan Rojas',35,NULL),(13,'Andrea Ramirez',33,NULL),(14,'Lizeth Pardo',23,NULL),(15,'Brayan Olivares',30,NULL),(16,'Yeison Trujillo',32,NULL),(17,'Jason Rodriguez',31,NULL),(18,'Gladys Torres',30,NULL),(19,'Liz Ramirez',32,NULL),(20,'Dayana Correa',33,NULL),(21,'Julieth Nieto',35,NULL),(22,'Brayan Rojas',35,NULL),(23,'Andrea Ramirez',33,NULL),(24,'Lizeth Pardo',23,NULL),(25,'Brayan Olivares',30,NULL),(26,'Yeison Trujillo',32,NULL),(27,'Jason Rodriguez',31,NULL),(28,'Gladys Torres',30,NULL),(29,'Liz Ramirez',32,NULL),(30,'Dayana Correa',33,NULL),(31,'Julieth Nieto',35,NULL),(32,'Brayan Rojas',35,NULL),(33,'Andrea Ramirez',33,NULL),(34,'Lizeth Pardo',23,NULL),(35,'Brayan Olivares',30,NULL),(36,'Yeison Trujillo',32,NULL),(37,'Jason Rodriguez',31,NULL),(38,'Gladys Torres',30,NULL),(39,'Liz Ramirez',32,NULL),(40,'Dayana Correa',33,NULL),(41,'Julieth Nieto',35,NULL),(42,'Brayan Rojas',35,NULL),(43,'Andrea Ramirez',33,NULL),(44,'Lizeth Pardo',23,NULL),(45,'Brayan Olivares',30,NULL),(46,'Yeison Trujillo',32,NULL),(47,'Jason Rodriguez',31,NULL),(48,'Gladys Torres',30,NULL),(49,'Liz Ramirez',32,NULL),(50,'Dayana Correa',33,NULL),(51,'Julieth Nieto',35,NULL);
/*!40000 ALTER TABLE `curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle`
--

DROP TABLE IF EXISTS `detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle` (
  `IdDetalle` bigint(20) NOT NULL AUTO_INCREMENT,
  `Fallas` bigint(20) DEFAULT NULL,
  `IdEstudiante` bigint(20) DEFAULT NULL,
  `IdAsistencia` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdDetalle`),
  KEY `DetalleAsistencia` (`IdAsistencia`),
  CONSTRAINT `detalle_ibfk_1` FOREIGN KEY (`IdAsistencia`) REFERENCES `asistencia` (`IdAsistencia`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle`
--

LOCK TABLES `detalle` WRITE;
/*!40000 ALTER TABLE `detalle` DISABLE KEYS */;
INSERT INTO `detalle` VALUES (1,1,NULL,NULL),(2,2,NULL,NULL),(3,3,NULL,NULL),(4,4,NULL,NULL),(5,5,NULL,NULL),(6,6,NULL,NULL),(7,7,NULL,NULL),(8,8,NULL,NULL),(9,9,NULL,NULL),(10,10,NULL,NULL),(11,1,NULL,NULL),(12,2,NULL,NULL),(13,3,NULL,NULL),(14,4,NULL,NULL),(15,5,NULL,NULL),(16,6,NULL,NULL),(17,7,NULL,NULL),(18,8,NULL,NULL),(19,9,NULL,NULL),(20,10,NULL,NULL),(21,1,NULL,NULL),(22,2,NULL,NULL),(23,3,NULL,NULL),(24,4,NULL,NULL),(25,5,NULL,NULL),(26,6,NULL,NULL),(27,7,NULL,NULL),(28,8,NULL,NULL),(29,9,NULL,NULL),(30,10,NULL,NULL),(31,1,NULL,NULL),(32,2,NULL,NULL),(33,3,NULL,NULL),(34,4,NULL,NULL),(35,5,NULL,NULL),(36,6,NULL,NULL),(37,7,NULL,NULL),(38,8,NULL,NULL),(39,9,NULL,NULL),(40,10,NULL,NULL),(41,1,NULL,NULL),(42,2,NULL,NULL),(43,3,NULL,NULL),(44,4,NULL,NULL),(45,5,NULL,NULL),(46,6,NULL,NULL),(47,7,NULL,NULL),(48,8,NULL,NULL),(49,9,NULL,NULL),(50,10,NULL,NULL);
/*!40000 ALTER TABLE `detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallece`
--

DROP TABLE IF EXISTS `detallece`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallece` (
  `IdDetalleCE` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdCalificacion` bigint(20) DEFAULT NULL,
  `IdEstudiante` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdDetalleCE`),
  KEY `CalificacionDetalle` (`IdCalificacion`),
  KEY `EstudianteDetalle` (`IdEstudiante`),
  CONSTRAINT `detallece_ibfk_1` FOREIGN KEY (`IdCalificacion`) REFERENCES `calificacion` (`IdCalificacion`),
  CONSTRAINT `detallece_ibfk_2` FOREIGN KEY (`IdEstudiante`) REFERENCES `estudiante` (`IdEstudiante`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallece`
--

LOCK TABLES `detallece` WRITE;
/*!40000 ALTER TABLE `detallece` DISABLE KEYS */;
INSERT INTO `detallece` VALUES (1,NULL,NULL),(2,NULL,NULL),(3,NULL,NULL),(4,NULL,NULL),(5,NULL,NULL),(6,NULL,NULL),(7,NULL,NULL),(8,NULL,NULL),(9,NULL,NULL),(10,NULL,NULL),(11,NULL,NULL),(12,NULL,NULL),(13,NULL,NULL),(14,NULL,NULL),(15,NULL,NULL),(16,NULL,NULL),(17,NULL,NULL),(18,NULL,NULL),(19,NULL,NULL),(20,NULL,NULL),(21,NULL,NULL),(22,NULL,NULL),(23,NULL,NULL),(24,NULL,NULL),(25,NULL,NULL),(26,NULL,NULL),(27,NULL,NULL),(28,NULL,NULL),(29,NULL,NULL),(30,NULL,NULL),(31,NULL,NULL),(32,NULL,NULL),(33,NULL,NULL),(34,NULL,NULL),(35,NULL,NULL),(36,NULL,NULL),(37,NULL,NULL),(38,NULL,NULL),(39,NULL,NULL),(40,NULL,NULL),(41,NULL,NULL),(42,NULL,NULL),(43,NULL,NULL),(44,NULL,NULL),(45,NULL,NULL),(46,NULL,NULL),(47,NULL,NULL),(48,NULL,NULL),(49,NULL,NULL),(50,NULL,NULL);
/*!40000 ALTER TABLE `detallece` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallecp`
--

DROP TABLE IF EXISTS `detallecp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallecp` (
  `IdDetalleCP` bigint(20) NOT NULL AUTO_INCREMENT,
  `Edad` bigint(20) DEFAULT NULL,
  `IdCalificacion` bigint(20) DEFAULT NULL,
  `IdProfesor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdDetalleCP`),
  KEY `DetalleCalificacion` (`IdCalificacion`),
  KEY `DetalleProfesor` (`IdProfesor`),
  CONSTRAINT `detallecp_ibfk_1` FOREIGN KEY (`IdCalificacion`) REFERENCES `calificacion` (`IdCalificacion`),
  CONSTRAINT `detallecp_ibfk_2` FOREIGN KEY (`IdProfesor`) REFERENCES `profesor` (`IdProfesor`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallecp`
--

LOCK TABLES `detallecp` WRITE;
/*!40000 ALTER TABLE `detallecp` DISABLE KEYS */;
INSERT INTO `detallecp` VALUES (1,15,NULL,NULL),(2,16,NULL,NULL),(3,17,NULL,NULL),(4,18,NULL,NULL),(5,14,NULL,NULL),(6,15,NULL,NULL),(7,16,NULL,NULL),(8,17,NULL,NULL),(9,18,NULL,NULL),(10,13,NULL,NULL),(11,15,NULL,NULL),(12,16,NULL,NULL),(13,17,NULL,NULL),(14,18,NULL,NULL),(15,14,NULL,NULL),(16,15,NULL,NULL),(17,16,NULL,NULL),(18,17,NULL,NULL),(19,18,NULL,NULL),(20,13,NULL,NULL),(21,15,NULL,NULL),(22,16,NULL,NULL),(23,17,NULL,NULL),(24,18,NULL,NULL),(25,14,NULL,NULL),(26,15,NULL,NULL),(27,16,NULL,NULL),(28,17,NULL,NULL),(29,18,NULL,NULL),(30,13,NULL,NULL),(31,15,NULL,NULL),(32,16,NULL,NULL),(33,17,NULL,NULL),(34,18,NULL,NULL),(35,14,NULL,NULL),(36,15,NULL,NULL),(37,16,NULL,NULL),(38,17,NULL,NULL),(39,18,NULL,NULL),(40,13,NULL,NULL),(41,15,NULL,NULL),(42,16,NULL,NULL),(43,17,NULL,NULL),(44,18,NULL,NULL),(45,14,NULL,NULL),(46,15,NULL,NULL),(47,16,NULL,NULL),(48,17,NULL,NULL),(49,18,NULL,NULL),(50,13,NULL,NULL);
/*!40000 ALTER TABLE `detallecp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `IdEstado` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IdEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` VALUES (1,'Activo'),(2,'Inactivo'),(3,'Activo'),(4,'Inactivo'),(5,'Activo'),(6,'Inactivo'),(7,'Activo'),(8,'Inactivo'),(9,'Activo'),(10,'Inactivo'),(11,'Activo'),(12,'Inactivo'),(13,'Activo'),(14,'Inactivo'),(15,'Activo'),(16,'Inactivo'),(17,'Activo'),(18,'Inactivo'),(19,'Activo'),(20,'Inactivo'),(21,'Activo'),(22,'Inactivo'),(23,'Activo'),(24,'Inactivo'),(25,'Activo'),(26,'Inactivo'),(27,'Activo'),(28,'Inactivo'),(29,'Activo'),(30,'Inactivo'),(31,'Activo'),(32,'Inactivo'),(33,'Activo'),(34,'Inactivo'),(35,'Activo'),(36,'Inactivo'),(37,'Activo'),(38,'Inactivo'),(39,'Activo'),(40,'Inactivo'),(41,'Activo'),(42,'Inactivo'),(43,'Activo'),(44,'Inactivo'),(45,'Activo'),(46,'Inactivo'),(47,'Activo'),(48,'Inactivo'),(49,'Activo'),(50,'Inactivo');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estudiante`
--

DROP TABLE IF EXISTS `estudiante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estudiante` (
  `IdEstudiante` bigint(20) NOT NULL AUTO_INCREMENT,
  `Edad` bigint(20) DEFAULT NULL,
  `Telefono` bigint(20) DEFAULT NULL,
  `Direccion` varchar(100) DEFAULT NULL,
  `IdEstado` bigint(20) DEFAULT NULL,
  `IdCurso` bigint(20) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IdEstudiante`),
  KEY `EstadoEstudiante` (`IdEstado`),
  KEY `EstudianteCurso` (`IdCurso`),
  CONSTRAINT `estudiante_ibfk_1` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`),
  CONSTRAINT `estudiante_ibfk_2` FOREIGN KEY (`IdCurso`) REFERENCES `estudiante` (`IdEstudiante`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estudiante`
--

LOCK TABLES `estudiante` WRITE;
/*!40000 ALTER TABLE `estudiante` DISABLE KEYS */;
INSERT INTO `estudiante` VALUES (1,16,12345,'av 12 n°56-76',NULL,NULL,'Brayan Rojas'),(2,17,67890,'av 23 n°56-76',NULL,NULL,'Dayana Correa'),(3,18,12345,'av 34 n°56-76',NULL,NULL,'Luis Lopez'),(4,15,67890,'av 45 n°56-76',NULL,NULL,'Angie caicedo'),(5,14,12345,'av 56 n°56-76',NULL,NULL,'Lorena Orosoco'),(6,16,67890,'av 67 n°56-76',NULL,NULL,'Brayan Olivares'),(7,17,12345,'av 78 n°56-76',NULL,NULL,'Jason Rodriguez'),(8,18,67890,'av 34 n°56-76',NULL,NULL,'Gladys Torres'),(9,14,12345,'av 45 n°56-76',NULL,NULL,'Hector Torres'),(10,13,67890,'av 56 n°56-76',NULL,NULL,'Diego Boada'),(11,16,12345,'av 12 n°56-76',NULL,NULL,'Brayan Rojas'),(12,17,67890,'av 23 n°56-76',NULL,NULL,'Dayana Correa'),(13,18,12345,'av 34 n°56-76',NULL,NULL,'Luis Lopez'),(14,15,67890,'av 45 n°56-76',NULL,NULL,'Angie caicedo'),(15,14,12345,'av 56 n°56-76',NULL,NULL,'Lorena Orosoco'),(16,16,67890,'av 67 n°56-76',NULL,NULL,'Brayan Olivares'),(17,17,12345,'av 78 n°56-76',NULL,NULL,'Jason Rodriguez'),(18,18,67890,'av 34 n°56-76',NULL,NULL,'Gladys Torres'),(19,14,12345,'av 45 n°56-76',NULL,NULL,'Hector Torres'),(20,13,67890,'av 56 n°56-76',NULL,NULL,'Diego Boada'),(21,16,12345,'av 12 n°56-76',NULL,NULL,'Brayan Rojas'),(22,17,67890,'av 23 n°56-76',NULL,NULL,'Dayana Correa'),(23,18,12345,'av 34 n°56-76',NULL,NULL,'Luis Lopez'),(24,15,67890,'av 45 n°56-76',NULL,NULL,'Angie caicedo'),(25,14,12345,'av 56 n°56-76',NULL,NULL,'Lorena Orosoco'),(26,16,67890,'av 67 n°56-76',NULL,NULL,'Brayan Olivares'),(27,17,12345,'av 78 n°56-76',NULL,NULL,'Jason Rodriguez'),(28,18,67890,'av 34 n°56-76',NULL,NULL,'Gladys Torres'),(29,14,12345,'av 45 n°56-76',NULL,NULL,'Hector Torres'),(30,13,67890,'av 56 n°56-76',NULL,NULL,'Diego Boada'),(31,16,12345,'av 12 n°56-76',NULL,NULL,'Brayan Rojas'),(32,17,67890,'av 23 n°56-76',NULL,NULL,'Dayana Correa'),(33,18,12345,'av 34 n°56-76',NULL,NULL,'Luis Lopez'),(34,15,67890,'av 45 n°56-76',NULL,NULL,'Angie caicedo'),(35,14,12345,'av 56 n°56-76',NULL,NULL,'Lorena Orosoco'),(36,16,67890,'av 67 n°56-76',NULL,NULL,'Brayan Olivares'),(37,17,12345,'av 78 n°56-76',NULL,NULL,'Jason Rodriguez'),(38,18,67890,'av 34 n°56-76',NULL,NULL,'Gladys Torres'),(39,14,12345,'av 45 n°56-76',NULL,NULL,'Hector Torres'),(40,13,67890,'av 56 n°56-76',NULL,NULL,'Diego Boada'),(41,16,12345,'av 12 n°56-76',NULL,NULL,'Brayan Rojas'),(42,17,67890,'av 23 n°56-76',NULL,NULL,'Dayana Correa'),(43,18,12345,'av 34 n°56-76',NULL,NULL,'Luis Lopez'),(44,15,67890,'av 45 n°56-76',NULL,NULL,'Angie caicedo'),(45,14,12345,'av 56 n°56-76',NULL,NULL,'Lorena Orosoco'),(46,16,67890,'av 67 n°56-76',NULL,NULL,'Brayan Olivares'),(47,17,12345,'av 78 n°56-76',NULL,NULL,'Jason Rodriguez'),(48,18,67890,'av 34 n°56-76',NULL,NULL,'Gladys Torres'),(49,14,12345,'av 45 n°56-76',NULL,NULL,'Hector Torres'),(50,13,67890,'av 56 n°56-76',NULL,NULL,'Diego Boada');
/*!40000 ALTER TABLE `estudiante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formacionacademica`
--

DROP TABLE IF EXISTS `formacionacademica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formacionacademica` (
  `IdFormacionAcademica` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdEstado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdFormacionAcademica`),
  KEY `EstadoFormacion` (`IdEstado`),
  CONSTRAINT `formacionacademica_ibfk_1` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formacionacademica`
--

LOCK TABLES `formacionacademica` WRITE;
/*!40000 ALTER TABLE `formacionacademica` DISABLE KEYS */;
INSERT INTO `formacionacademica` VALUES (1,NULL),(2,NULL),(3,NULL),(4,NULL),(5,NULL),(6,NULL),(7,NULL),(8,NULL),(9,NULL),(10,NULL),(11,NULL),(12,NULL),(13,NULL),(14,NULL),(15,NULL),(16,NULL),(17,NULL),(18,NULL),(19,NULL),(20,NULL),(21,NULL),(22,NULL),(23,NULL),(24,NULL),(25,NULL),(26,NULL),(27,NULL),(28,NULL),(29,NULL),(30,NULL),(31,NULL),(32,NULL),(33,NULL),(34,NULL),(35,NULL),(36,NULL),(37,NULL),(38,NULL),(39,NULL),(40,NULL),(41,NULL),(42,NULL),(43,NULL),(44,NULL),(45,NULL),(46,NULL),(47,NULL),(48,NULL),(49,NULL),(50,NULL);
/*!40000 ALTER TABLE `formacionacademica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inscripcion`
--

DROP TABLE IF EXISTS `inscripcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inscripcion` (
  `IdInscripcion` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreAlumno` varchar(100) DEFAULT NULL,
  `Tipo` varchar(100) DEFAULT NULL,
  `IdEstado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdInscripcion`),
  KEY `EstadoInscripcion` (`IdEstado`),
  CONSTRAINT `inscripcion_ibfk_1` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inscripcion`
--

LOCK TABLES `inscripcion` WRITE;
/*!40000 ALTER TABLE `inscripcion` DISABLE KEYS */;
INSERT INTO `inscripcion` VALUES (1,'Brayan Rojas','Alumno',NULL),(2,'Brayan Olivares','Alumno',NULL),(3,'Paola Cardenas','Alumno',NULL),(4,'Dayana Correa','Alumno',NULL),(5,'Anna Quiñones','Alumno',NULL),(6,'Sara Bernal','Alumno',NULL),(7,'Felipe Larrota','Alumno',NULL),(8,'Jason Rodriguez','Alumno',NULL),(9,'Kevin Manzano','Alumno',NULL),(10,'Juan Rojas','Alumno',NULL),(11,'Brayan Rojas','Alumno',NULL),(12,'Brayan Olivares','Alumno',NULL),(13,'Paola Cardenas','Alumno',NULL),(14,'Dayana Correa','Alumno',NULL),(15,'Anna Quiñones','Alumno',NULL),(16,'Sara Bernal','Alumno',NULL),(17,'Felipe Larrota','Alumno',NULL),(18,'Jason Rodriguez','Alumno',NULL),(19,'Kevin Manzano','Alumno',NULL),(20,'Juan Rojas','Alumno',NULL),(21,'Brayan Rojas','Alumno',NULL),(22,'Brayan Olivares','Alumno',NULL),(23,'Paola Cardenas','Alumno',NULL),(24,'Dayana Correa','Alumno',NULL),(25,'Anna Quiñones','Alumno',NULL),(26,'Sara Bernal','Alumno',NULL),(27,'Felipe Larrota','Alumno',NULL),(28,'Jason Rodriguez','Alumno',NULL),(29,'Kevin Manzano','Alumno',NULL),(30,'Juan Rojas','Alumno',NULL),(31,'Brayan Rojas','Alumno',NULL),(32,'Brayan Olivares','Alumno',NULL),(33,'Paola Cardenas','Alumno',NULL),(34,'Dayana Correa','Alumno',NULL),(35,'Anna Quiñones','Alumno',NULL),(36,'Sara Bernal','Alumno',NULL),(37,'Felipe Larrota','Alumno',NULL),(38,'Jason Rodriguez','Alumno',NULL),(39,'Kevin Manzano','Alumno',NULL),(40,'Juan Rojas','Alumno',NULL),(41,'Brayan Rojas','Alumno',NULL),(42,'Brayan Olivares','Alumno',NULL),(43,'Paola Cardenas','Alumno',NULL),(44,'Dayana Correa','Alumno',NULL),(45,'Anna Quiñones','Alumno',NULL),(46,'Sara Bernal','Alumno',NULL),(47,'Felipe Larrota','Alumno',NULL),(48,'Jason Rodriguez','Alumno',NULL),(49,'Kevin Manzano','Alumno',NULL),(50,'Juan Rojas','Alumno',NULL);
/*!40000 ALTER TABLE `inscripcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profesor`
--

DROP TABLE IF EXISTS `profesor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profesor` (
  `IdProfesor` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `Direccion` varchar(100) DEFAULT NULL,
  `Telefono` bigint(20) DEFAULT NULL,
  `IdEstado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdProfesor`),
  KEY `EstadoProfesor` (`IdEstado`),
  CONSTRAINT `profesor_ibfk_1` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profesor`
--

LOCK TABLES `profesor` WRITE;
/*!40000 ALTER TABLE `profesor` DISABLE KEYS */;
INSERT INTO `profesor` VALUES (1,'Brayan Rojas','av12 n°12-23',12345,NULL),(2,'Juan perez','av23 n°12-23',67890,NULL),(3,'alejandro munevar','av34 n°12-23',67890,NULL),(4,'jorge pedraza','av45 n°12-23',67890,NULL),(5,'humberto ramirez','av56 n°12-23',12345,NULL),(6,'grabiel torres','av12 n°12-23',12345,NULL),(7,'sebasian perez','av23 n°12-23',67890,NULL),(8,'hector mora','av34 n°12-23',12345,NULL),(9,'martha rojas','av45 n°12-23',67890,NULL),(10,'jhon matias','av12 n°12-23',12345,NULL),(11,'Brayan Rojas','av12 n°12-23',12345,NULL),(12,'Juan perez','av23 n°12-23',67890,NULL),(13,'alejandro munevar','av34 n°12-23',67890,NULL),(14,'jorge pedraza','av45 n°12-23',67890,NULL),(15,'humberto ramirez','av56 n°12-23',12345,NULL),(16,'grabiel torres','av12 n°12-23',12345,NULL),(17,'sebasian perez','av23 n°12-23',67890,NULL),(18,'hector mora','av34 n°12-23',12345,NULL),(19,'martha rojas','av45 n°12-23',67890,NULL),(20,'jhon matias','av12 n°12-23',12345,NULL),(21,'Brayan Rojas','av12 n°12-23',12345,NULL),(22,'Juan perez','av23 n°12-23',67890,NULL),(23,'alejandro munevar','av34 n°12-23',67890,NULL),(24,'jorge pedraza','av45 n°12-23',67890,NULL),(25,'humberto ramirez','av56 n°12-23',12345,NULL),(26,'grabiel torres','av12 n°12-23',12345,NULL),(27,'sebasian perez','av23 n°12-23',67890,NULL),(28,'hector mora','av34 n°12-23',12345,NULL),(29,'martha rojas','av45 n°12-23',67890,NULL),(30,'jhon matias','av12 n°12-23',12345,NULL),(31,'Brayan Rojas','av12 n°12-23',12345,NULL),(32,'Juan perez','av23 n°12-23',67890,NULL),(33,'alejandro munevar','av34 n°12-23',67890,NULL),(34,'jorge pedraza','av45 n°12-23',67890,NULL),(35,'humberto ramirez','av56 n°12-23',12345,NULL),(36,'grabiel torres','av12 n°12-23',12345,NULL),(37,'sebasian perez','av23 n°12-23',67890,NULL),(38,'hector mora','av34 n°12-23',12345,NULL),(39,'martha rojas','av45 n°12-23',67890,NULL),(40,'jhon matias','av12 n°12-23',12345,NULL),(41,'Brayan Rojas','av12 n°12-23',12345,NULL),(42,'Juan perez','av23 n°12-23',67890,NULL),(43,'alejandro munevar','av34 n°12-23',67890,NULL),(44,'jorge pedraza','av45 n°12-23',67890,NULL),(45,'humberto ramirez','av56 n°12-23',12345,NULL),(46,'grabiel torres','av12 n°12-23',12345,NULL),(47,'sebasian perez','av23 n°12-23',67890,NULL),(48,'hector mora','av34 n°12-23',12345,NULL),(49,'martha rojas','av45 n°12-23',67890,NULL),(50,'jhon matias','av12 n°12-23',12345,NULL);
/*!40000 ALTER TABLE `profesor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoea`
--

DROP TABLE IF EXISTS `tipoea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoea` (
  `IdTipoAE` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdEstudiante` bigint(20) DEFAULT NULL,
  `IdAsignatura` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdTipoAE`),
  KEY `EstudianteTipo` (`IdEstudiante`),
  KEY `AsignaturaTipo` (`IdAsignatura`),
  CONSTRAINT `tipoea_ibfk_1` FOREIGN KEY (`IdEstudiante`) REFERENCES `estudiante` (`IdEstudiante`),
  CONSTRAINT `tipoea_ibfk_2` FOREIGN KEY (`IdAsignatura`) REFERENCES `asignatura` (`IdAsignatura`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoea`
--

LOCK TABLES `tipoea` WRITE;
/*!40000 ALTER TABLE `tipoea` DISABLE KEYS */;
INSERT INTO `tipoea` VALUES (1,NULL,NULL),(2,NULL,NULL),(3,NULL,NULL),(4,NULL,NULL),(5,NULL,NULL),(6,NULL,NULL),(7,NULL,NULL),(8,NULL,NULL),(9,NULL,NULL),(10,NULL,NULL),(11,NULL,NULL),(12,NULL,NULL),(13,NULL,NULL),(14,NULL,NULL),(15,NULL,NULL),(16,NULL,NULL),(17,NULL,NULL),(18,NULL,NULL),(19,NULL,NULL),(20,NULL,NULL),(21,NULL,NULL),(22,NULL,NULL),(23,NULL,NULL),(24,NULL,NULL),(25,NULL,NULL),(26,NULL,NULL),(27,NULL,NULL),(28,NULL,NULL),(29,NULL,NULL),(30,NULL,NULL),(31,NULL,NULL),(32,NULL,NULL),(33,NULL,NULL),(34,NULL,NULL),(35,NULL,NULL),(36,NULL,NULL),(37,NULL,NULL),(38,NULL,NULL),(39,NULL,NULL),(40,NULL,NULL),(41,NULL,NULL),(42,NULL,NULL),(43,NULL,NULL),(44,NULL,NULL),(45,NULL,NULL),(46,NULL,NULL),(47,NULL,NULL),(48,NULL,NULL),(49,NULL,NULL),(50,NULL,NULL);
/*!40000 ALTER TABLE `tipoea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoef`
--

DROP TABLE IF EXISTS `tipoef`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoef` (
  `IdTipoEF` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `IdFormacionAcademica` bigint(20) DEFAULT NULL,
  `IdEstudiante` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdTipoEF`),
  KEY `TipoFormacion` (`IdFormacionAcademica`),
  KEY `TipoEstudiante` (`IdEstudiante`),
  CONSTRAINT `tipoef_ibfk_2` FOREIGN KEY (`IdEstudiante`) REFERENCES `estudiante` (`IdEstudiante`),
  CONSTRAINT `tipoef_ibfk_1` FOREIGN KEY (`IdFormacionAcademica`) REFERENCES `formacionacademica` (`IdFormacionAcademica`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoef`
--

LOCK TABLES `tipoef` WRITE;
/*!40000 ALTER TABLE `tipoef` DISABLE KEYS */;
INSERT INTO `tipoef` VALUES (1,'brayan rojas',NULL,NULL),(2,'Juan Mata',NULL,NULL),(3,'James rodriguez',NULL,NULL),(4,'cristiano ronaldo',NULL,NULL),(5,'Juanito pereez',NULL,NULL),(6,'Juan mata',NULL,NULL),(7,'Fredy torres',NULL,NULL),(8,'Jason rodriguez',NULL,NULL),(9,'Anna quiñones',NULL,NULL),(10,'Juan Rojas',NULL,NULL),(11,'brayan rojas',NULL,NULL),(12,'Juan Mata',NULL,NULL),(13,'James rodriguez',NULL,NULL),(14,'cristiano ronaldo',NULL,NULL),(15,'Juanito pereez',NULL,NULL),(16,'Juan mata',NULL,NULL),(17,'Fredy torres',NULL,NULL),(18,'Jason rodriguez',NULL,NULL),(19,'Anna quiñones',NULL,NULL),(20,'Juan Rojas',NULL,NULL),(21,'brayan rojas',NULL,NULL),(22,'Juan Mata',NULL,NULL),(23,'James rodriguez',NULL,NULL),(24,'cristiano ronaldo',NULL,NULL),(25,'Juanito pereez',NULL,NULL),(26,'Juan mata',NULL,NULL),(27,'Fredy torres',NULL,NULL),(28,'Jason rodriguez',NULL,NULL),(29,'Anna quiñones',NULL,NULL),(30,'Juan Rojas',NULL,NULL),(31,'brayan rojas',NULL,NULL),(32,'Juan Mata',NULL,NULL),(33,'James rodriguez',NULL,NULL),(34,'cristiano ronaldo',NULL,NULL),(35,'Juanito pereez',NULL,NULL),(36,'Juan mata',NULL,NULL),(37,'Fredy torres',NULL,NULL),(38,'Jason rodriguez',NULL,NULL),(39,'Anna quiñones',NULL,NULL),(40,'Juan Rojas',NULL,NULL),(41,'brayan rojas',NULL,NULL),(42,'Juan Mata',NULL,NULL),(43,'James rodriguez',NULL,NULL),(44,'cristiano ronaldo',NULL,NULL),(45,'Juanito pereez',NULL,NULL),(46,'Juan mata',NULL,NULL),(47,'Fredy torres',NULL,NULL),(48,'Jason rodriguez',NULL,NULL),(49,'Anna quiñones',NULL,NULL),(50,'Juan Rojas',NULL,NULL);
/*!40000 ALTER TABLE `tipoef` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-18 12:30:43
