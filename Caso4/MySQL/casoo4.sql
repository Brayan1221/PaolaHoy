-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: caso4
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `IdCliente` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `Identificacion` bigint(20) DEFAULT NULL,
  `Telefono` bigint(20) DEFAULT NULL,
  `Direccion` varchar(100) DEFAULT NULL,
  `IdEstado` bigint(20) DEFAULT NULL,
  `IdContrato` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdCliente`),
  KEY `ClienteEstado` (`IdEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,'Brayan Rojas',111222333,12345,'av 45 n°45-78',NULL,NULL),(2,'Anna Quiñones',111222333,12345,'av 45 n°45-78',NULL,NULL),(3,'Sara Bernal',444555666,12345,'av 45 n°45-78',NULL,NULL),(4,'Gabiel ascanio',777888999,12345,'av 45 n°45-78',NULL,NULL),(5,'Jhonny Guarnizo',444555666,12345,'av 45 n°45-78',NULL,NULL),(6,'Daniela Vargas',444555666,12345,'av 45 n°45-78',NULL,NULL),(7,'Javier Moreno',777888999,12345,'av 45 n°45-78',NULL,NULL),(8,'Jhonatan Torres',444555666,12345,'av 45 n°45-78',NULL,NULL),(9,'Nicol martines',444555666,12345,'av 45 n°45-78',NULL,NULL),(10,'Gladys Torres',111222333,12345,'av 45 n°45-78',NULL,NULL),(11,'Juan Mendez',444555666,12345,'av 45 n°45-78',NULL,NULL),(12,'Anna Quiñones',111222333,12345,'av 45 n°45-78',NULL,NULL),(13,'Sara Bernal',444555666,12345,'av 45 n°45-78',NULL,NULL),(14,'Gabiel ascanio',777888999,12345,'av 45 n°45-78',NULL,NULL),(15,'Jhonny Guarnizo',444555666,12345,'av 45 n°45-78',NULL,NULL),(16,'Daniela Vargas',444555666,12345,'av 45 n°45-78',NULL,NULL),(17,'Javier Moreno',777888999,12345,'av 45 n°45-78',NULL,NULL),(18,'Jhonatan Torres',444555666,12345,'av 45 n°45-78',NULL,NULL),(19,'Nicol martines',444555666,12345,'av 45 n°45-78',NULL,NULL),(20,'Gladys Torres',111222333,12345,'av 45 n°45-78',NULL,NULL),(21,'Juan Mendez',444555666,12345,'av 45 n°45-78',NULL,NULL),(22,'Anna Quiñones',111222333,12345,'av 45 n°45-78',NULL,NULL),(23,'Sara Bernal',444555666,12345,'av 45 n°45-78',NULL,NULL),(24,'Gabiel ascanio',777888999,12345,'av 45 n°45-78',NULL,NULL),(25,'Jhonny Guarnizo',444555666,12345,'av 45 n°45-78',NULL,NULL),(26,'Daniela Vargas',444555666,12345,'av 45 n°45-78',NULL,NULL),(27,'Javier Moreno',777888999,12345,'av 45 n°45-78',NULL,NULL),(28,'Jhonatan Torres',444555666,12345,'av 45 n°45-78',NULL,NULL),(29,'Nicol martines',444555666,12345,'av 45 n°45-78',NULL,NULL),(30,'Gladys Torres',111222333,12345,'av 45 n°45-78',NULL,NULL),(31,'Juan Mendez',444555666,12345,'av 45 n°45-78',NULL,NULL),(32,'Anna Quiñones',111222333,12345,'av 45 n°45-78',NULL,NULL),(33,'Sara Bernal',444555666,12345,'av 45 n°45-78',NULL,NULL),(34,'Gabiel ascanio',777888999,12345,'av 45 n°45-78',NULL,NULL),(35,'Jhonny Guarnizo',444555666,12345,'av 45 n°45-78',NULL,NULL),(36,'Daniela Vargas',444555666,12345,'av 45 n°45-78',NULL,NULL),(37,'Javier Moreno',777888999,12345,'av 45 n°45-78',NULL,NULL),(38,'Jhonatan Torres',444555666,12345,'av 45 n°45-78',NULL,NULL),(39,'Nicol martines',444555666,12345,'av 45 n°45-78',NULL,NULL),(40,'Gladys Torres',111222333,12345,'av 45 n°45-78',NULL,NULL),(41,'Juan Mendez',444555666,12345,'av 45 n°45-78',NULL,NULL),(42,'Anna Quiñones',111222333,12345,'av 45 n°45-78',NULL,NULL),(43,'Sara Bernal',444555666,12345,'av 45 n°45-78',NULL,NULL),(44,'Gabiel ascanio',777888999,12345,'av 45 n°45-78',NULL,NULL),(45,'Jhonny Guarnizo',444555666,12345,'av 45 n°45-78',NULL,NULL),(46,'Daniela Vargas',444555666,12345,'av 45 n°45-78',NULL,NULL),(47,'Javier Moreno',777888999,12345,'av 45 n°45-78',NULL,NULL),(48,'Jhonatan Torres',444555666,12345,'av 45 n°45-78',NULL,NULL),(49,'Nicol martines',444555666,12345,'av 45 n°45-78',NULL,NULL),(50,'Gladys Torres',111222333,12345,'av 45 n°45-78',NULL,NULL);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clienteempleado`
--

DROP TABLE IF EXISTS `clienteempleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clienteempleado` (
  `IdClienteEmpleado` bigint(20) NOT NULL AUTO_INCREMENT,
  `Cargo` varchar(100) DEFAULT NULL,
  `Edad` bigint(20) DEFAULT NULL,
  `IdCliente` bigint(20) DEFAULT NULL,
  `IdEmpleado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdClienteEmpleado`),
  KEY `ClienteEmpleado` (`IdCliente`),
  KEY `EmpleadoCliente` (`IdEmpleado`),
  CONSTRAINT `clienteempleado_ibfk_2` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`IdEmpleado`),
  CONSTRAINT `clienteempleado_ibfk_1` FOREIGN KEY (`IdCliente`) REFERENCES `cliente` (`IdCliente`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clienteempleado`
--

LOCK TABLES `clienteempleado` WRITE;
/*!40000 ALTER TABLE `clienteempleado` DISABLE KEYS */;
INSERT INTO `clienteempleado` VALUES (1,'Vendedor',32,NULL,NULL),(2,'jefe bodega',21,NULL,NULL),(3,'aseo',45,NULL,NULL),(4,'gerente',43,NULL,NULL),(5,'jefe directo',12,NULL,NULL),(6,'asesor',67,NULL,NULL),(7,'celado',34,NULL,NULL),(8,'diseñador',12,NULL,NULL),(9,'asistente',34,NULL,NULL),(10,'mensajeria',54,NULL,NULL),(11,'Vendedor',32,NULL,NULL),(12,'jefe bodega',21,NULL,NULL),(13,'aseo',45,NULL,NULL),(14,'gerente',43,NULL,NULL),(15,'jefe directo',12,NULL,NULL),(16,'asesor',67,NULL,NULL),(17,'celado',34,NULL,NULL),(18,'diseñador',12,NULL,NULL),(19,'asistente',34,NULL,NULL),(20,'mensajeria',54,NULL,NULL),(21,'Vendedor',32,NULL,NULL),(22,'jefe bodega',21,NULL,NULL),(23,'aseo',45,NULL,NULL),(24,'gerente',43,NULL,NULL),(25,'jefe directo',12,NULL,NULL),(26,'asesor',67,NULL,NULL),(27,'celado',34,NULL,NULL),(28,'diseñador',12,NULL,NULL),(29,'asistente',34,NULL,NULL),(30,'mensajeria',54,NULL,NULL),(31,'Vendedor',32,NULL,NULL),(32,'jefe bodega',21,NULL,NULL),(33,'aseo',45,NULL,NULL),(34,'gerente',43,NULL,NULL),(35,'jefe directo',12,NULL,NULL),(36,'asesor',67,NULL,NULL),(37,'celado',34,NULL,NULL),(38,'diseñador',12,NULL,NULL),(39,'asistente',34,NULL,NULL),(40,'mensajeria',54,NULL,NULL),(41,'Vendedor',32,NULL,NULL),(42,'jefe bodega',21,NULL,NULL),(43,'aseo',45,NULL,NULL),(44,'gerente',43,NULL,NULL),(45,'jefe directo',12,NULL,NULL),(46,'asesor',67,NULL,NULL),(47,'celado',34,NULL,NULL),(48,'diseñador',12,NULL,NULL),(49,'asistente',34,NULL,NULL),(50,'mensajeria',54,NULL,NULL);
/*!40000 ALTER TABLE `clienteempleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrato`
--

DROP TABLE IF EXISTS `contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrato` (
  `IdContrato` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreCliente` varchar(100) DEFAULT NULL,
  `FechaInicial` date DEFAULT NULL,
  `FechaFinal` date DEFAULT NULL,
  `IdEstado` bigint(20) DEFAULT NULL,
  `IdCliente` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdContrato`),
  KEY `ContratoEstado` (`IdEstado`),
  KEY `ContratoCliente` (`IdCliente`),
  CONSTRAINT `contrato_ibfk_2` FOREIGN KEY (`IdCliente`) REFERENCES `cliente` (`IdCliente`),
  CONSTRAINT `contrato_ibfk_1` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato`
--

LOCK TABLES `contrato` WRITE;
/*!40000 ALTER TABLE `contrato` DISABLE KEYS */;
INSERT INTO `contrato` VALUES (1,'Brayan Rojas','2016-04-05','2016-04-01',NULL,NULL),(2,'Brayan Rojas','2016-04-05','2016-04-01',NULL,NULL),(3,'Yurany martinez','2013-04-05','2016-04-01',NULL,NULL),(4,'juan bravo','2012-04-05','2016-04-01',NULL,NULL),(5,'felipe rojas','2011-04-05','2016-04-01',NULL,NULL),(6,'sebastian ruiz','2010-04-05','2016-04-01',NULL,NULL),(7,'hector torres','2016-04-05','2016-04-01',NULL,NULL),(8,'martha torres','2015-04-05','2016-04-01',NULL,NULL),(9,'gladys torres','2014-04-05','2016-04-01',NULL,NULL),(10,'sara valeria','2013-04-05','2016-04-01',NULL,NULL),(11,'megan fox','2012-04-05','2016-04-01',NULL,NULL),(12,'Brayan Rojas','2016-04-05','2016-04-01',NULL,NULL),(13,'Yurany martinez','2013-04-05','2016-04-01',NULL,NULL),(14,'juan bravo','2012-04-05','2016-04-01',NULL,NULL),(15,'felipe rojas','2011-04-05','2016-04-01',NULL,NULL),(16,'sebastian ruiz','2010-04-05','2016-04-01',NULL,NULL),(17,'hector torres','2016-04-05','2016-04-01',NULL,NULL),(18,'martha torres','2015-04-05','2016-04-01',NULL,NULL),(19,'gladys torres','2014-04-05','2016-04-01',NULL,NULL),(20,'sara valeria','2013-04-05','2016-04-01',NULL,NULL),(21,'megan fox','2012-04-05','2016-04-01',NULL,NULL),(22,'Brayan Rojas','2016-04-05','2016-04-01',NULL,NULL),(23,'Yurany martinez','2013-04-05','2016-04-01',NULL,NULL),(24,'juan bravo','2012-04-05','2016-04-01',NULL,NULL),(25,'felipe rojas','2011-04-05','2016-04-01',NULL,NULL),(26,'sebastian ruiz','2010-04-05','2016-04-01',NULL,NULL),(27,'hector torres','2016-04-05','2016-04-01',NULL,NULL),(28,'martha torres','2015-04-05','2016-04-01',NULL,NULL),(29,'gladys torres','2014-04-05','2016-04-01',NULL,NULL),(30,'sara valeria','2013-04-05','2016-04-01',NULL,NULL),(31,'megan fox','2012-04-05','2016-04-01',NULL,NULL),(32,'Brayan Rojas','2016-04-05','2016-04-01',NULL,NULL),(33,'Yurany martinez','2013-04-05','2016-04-01',NULL,NULL),(34,'juan bravo','2012-04-05','2016-04-01',NULL,NULL),(35,'felipe rojas','2011-04-05','2016-04-01',NULL,NULL),(36,'sebastian ruiz','2010-04-05','2016-04-01',NULL,NULL),(37,'hector torres','2016-04-05','2016-04-01',NULL,NULL),(38,'martha torres','2015-04-05','2016-04-01',NULL,NULL),(39,'gladys torres','2014-04-05','2016-04-01',NULL,NULL),(40,'sara valeria','2013-04-05','2016-04-01',NULL,NULL),(41,'megan fox','2012-04-05','2016-04-01',NULL,NULL),(42,'Brayan Rojas','2016-04-05','2016-04-01',NULL,NULL),(43,'Yurany martinez','2013-04-05','2016-04-01',NULL,NULL),(44,'juan bravo','2012-04-05','2016-04-01',NULL,NULL),(45,'felipe rojas','2011-04-05','2016-04-01',NULL,NULL),(46,'sebastian ruiz','2010-04-05','2016-04-01',NULL,NULL),(47,'hector torres','2016-04-05','2016-04-01',NULL,NULL),(48,'martha torres','2015-04-05','2016-04-01',NULL,NULL),(49,'gladys torres','2014-04-05','2016-04-01',NULL,NULL),(50,'sara valeria','2013-04-05','2016-04-01',NULL,NULL);
/*!40000 ALTER TABLE `contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle`
--

DROP TABLE IF EXISTS `detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle` (
  `IdDetalle` bigint(20) NOT NULL AUTO_INCREMENT,
  `ValorUnitario` bigint(20) DEFAULT NULL,
  `ValorTotal` bigint(20) DEFAULT NULL,
  `Iva` bigint(20) DEFAULT NULL,
  `IdFacturaVenta` bigint(20) DEFAULT NULL,
  `IdProducto` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdDetalle`),
  KEY `FacturaDetalle` (`IdFacturaVenta`),
  KEY `DetalleProducto` (`IdProducto`),
  CONSTRAINT `detalle_ibfk_2` FOREIGN KEY (`IdProducto`) REFERENCES `producto` (`IdProducto`),
  CONSTRAINT `detalle_ibfk_1` FOREIGN KEY (`IdFacturaVenta`) REFERENCES `facturaventa` (`IdFacturaVenta`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle`
--

LOCK TABLES `detalle` WRITE;
/*!40000 ALTER TABLE `detalle` DISABLE KEYS */;
INSERT INTO `detalle` VALUES (1,1000,2000,500,NULL,NULL),(2,2000,3000,600,NULL,NULL),(3,3000,4000,700,NULL,NULL),(4,4000,5000,800,NULL,NULL),(5,5000,6000,900,NULL,NULL),(6,6000,7000,100,NULL,NULL),(7,7000,8000,200,NULL,NULL),(8,8000,9000,300,NULL,NULL),(9,9000,10000,400,NULL,NULL),(10,1000,2000,500,NULL,NULL),(11,1000,2000,500,NULL,NULL),(12,2000,3000,600,NULL,NULL),(13,3000,4000,700,NULL,NULL),(14,4000,5000,800,NULL,NULL),(15,5000,6000,900,NULL,NULL),(16,6000,7000,100,NULL,NULL),(17,7000,8000,200,NULL,NULL),(18,8000,9000,300,NULL,NULL),(19,9000,10000,400,NULL,NULL),(20,1000,2000,500,NULL,NULL),(21,1000,2000,500,NULL,NULL),(22,2000,3000,600,NULL,NULL),(23,3000,4000,700,NULL,NULL),(24,4000,5000,800,NULL,NULL),(25,5000,6000,900,NULL,NULL),(26,6000,7000,100,NULL,NULL),(27,7000,8000,200,NULL,NULL),(28,8000,9000,300,NULL,NULL),(29,9000,10000,400,NULL,NULL),(30,1000,2000,500,NULL,NULL),(31,1000,2000,500,NULL,NULL),(32,2000,3000,600,NULL,NULL),(33,3000,4000,700,NULL,NULL),(34,4000,5000,800,NULL,NULL),(35,5000,6000,900,NULL,NULL),(36,6000,7000,100,NULL,NULL),(37,7000,8000,200,NULL,NULL),(38,8000,9000,300,NULL,NULL),(39,9000,10000,400,NULL,NULL),(40,1000,2000,500,NULL,NULL),(41,1000,2000,500,NULL,NULL),(42,2000,3000,600,NULL,NULL),(43,3000,4000,700,NULL,NULL),(44,4000,5000,800,NULL,NULL),(45,5000,6000,900,NULL,NULL),(46,6000,7000,100,NULL,NULL),(47,7000,8000,200,NULL,NULL),(48,8000,9000,300,NULL,NULL),(49,9000,10000,400,NULL,NULL),(50,1000,2000,500,NULL,NULL);
/*!40000 ALTER TABLE `detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `IdEmpleado` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `Identificacion` bigint(20) DEFAULT NULL,
  `Telefono` bigint(20) DEFAULT NULL,
  `Direccion` varchar(100) DEFAULT NULL,
  `IdEstado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdEmpleado`),
  KEY `EmpleadoEstado` (`IdEstado`),
  CONSTRAINT `empleado_ibfk_1` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES (1,'Brayan Rojas',111222333,12345,'av 45 n°43-67',NULL),(2,'Jason Rodriguez',444555666,67890,'av 65 n°43-67',NULL),(3,'Gladys Torres',777888999,12345,'av 76 n°43-67',NULL),(4,'Hector Torres',111222333,67890,'av 23 n°43-67',NULL),(5,'Anna Quiñones',777888999,12345,'av 54 n°43-67',NULL),(6,'Sara Bernal',111222333,67890,'av 67 n°43-67',NULL),(7,'Felipe Rojas',777888999,12345,'av 34 n°43-67',NULL),(8,'Sebastian Ruiz',444555666,67890,'av 23 n°43-67',NULL),(9,'Miryam Torres',111222333,12345,'av 45 n°43-67',NULL),(10,'Nicolas Manzano',777888999,67890,'av 23 n°43-67',NULL),(11,'Brayan Rojas',111222333,12345,'av 23 n°43-67',NULL),(12,'Jason Rodriguez',444555666,67890,'av 65 n°43-67',NULL),(13,'Gladys Torres',777888999,12345,'av 76 n°43-67',NULL),(14,'Hector Torres',111222333,67890,'av 23 n°43-67',NULL),(15,'Anna Quiñones',777888999,12345,'av 54 n°43-67',NULL),(16,'Sara Bernal',111222333,67890,'av 67 n°43-67',NULL),(17,'Felipe Rojas',777888999,12345,'av 34 n°43-67',NULL),(18,'Sebastian Ruiz',444555666,67890,'av 23 n°43-67',NULL),(19,'Miryam Torres',111222333,12345,'av 45 n°43-67',NULL),(20,'Nicolas Manzano',777888999,67890,'av 23 n°43-67',NULL),(21,'Brayan Rojas',111222333,12345,'av 23 n°43-67',NULL),(22,'Jason Rodriguez',444555666,67890,'av 65 n°43-67',NULL),(23,'Gladys Torres',777888999,12345,'av 76 n°43-67',NULL),(24,'Hector Torres',111222333,67890,'av 23 n°43-67',NULL),(25,'Anna Quiñones',777888999,12345,'av 54 n°43-67',NULL),(26,'Sara Bernal',111222333,67890,'av 67 n°43-67',NULL),(27,'Felipe Rojas',777888999,12345,'av 34 n°43-67',NULL),(28,'Sebastian Ruiz',444555666,67890,'av 23 n°43-67',NULL),(29,'Miryam Torres',111222333,12345,'av 45 n°43-67',NULL),(30,'Nicolas Manzano',777888999,67890,'av 23 n°43-67',NULL),(31,'Brayan Rojas',111222333,12345,'av 23 n°43-67',NULL),(32,'Jason Rodriguez',444555666,67890,'av 65 n°43-67',NULL),(33,'Gladys Torres',777888999,12345,'av 76 n°43-67',NULL),(34,'Hector Torres',111222333,67890,'av 23 n°43-67',NULL),(35,'Anna Quiñones',777888999,12345,'av 54 n°43-67',NULL),(36,'Sara Bernal',111222333,67890,'av 67 n°43-67',NULL),(37,'Felipe Rojas',777888999,12345,'av 34 n°43-67',NULL),(38,'Sebastian Ruiz',444555666,67890,'av 23 n°43-67',NULL),(39,'Miryam Torres',111222333,12345,'av 45 n°43-67',NULL),(40,'Nicolas Manzano',777888999,67890,'av 23 n°43-67',NULL),(41,'Brayan Rojas',111222333,12345,'av 23 n°43-67',NULL),(42,'Jason Rodriguez',444555666,67890,'av 65 n°43-67',NULL),(43,'Gladys Torres',777888999,12345,'av 76 n°43-67',NULL),(44,'Hector Torres',111222333,67890,'av 23 n°43-67',NULL),(45,'Anna Quiñones',777888999,12345,'av 54 n°43-67',NULL),(46,'Sara Bernal',111222333,67890,'av 67 n°43-67',NULL),(47,'Felipe Rojas',777888999,12345,'av 34 n°43-67',NULL),(48,'Sebastian Ruiz',444555666,67890,'av 23 n°43-67',NULL),(49,'Miryam Torres',111222333,12345,'av 45 n°43-67',NULL),(50,'Nicolas Manzano',777888999,67890,'av 23 n°43-67',NULL);
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `IdEstado` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IdEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` VALUES (1,'Activo'),(2,'Inactivo'),(3,'Activo'),(4,'Inactivo'),(5,'Activo'),(6,'Inactivo'),(7,'Activo'),(8,'Inactivo'),(9,'Activo'),(10,'Inactivo'),(11,'Activo'),(12,'Inactivo'),(13,'Activo'),(14,'Inactivo'),(15,'Activo'),(16,'Inactivo'),(17,'Activo'),(18,'Inactivo'),(19,'Activo'),(20,'Inactivo'),(21,'Activo'),(22,'Inactivo'),(23,'Activo'),(24,'Inactivo'),(25,'Activo'),(26,'Inactivo'),(27,'Activo'),(28,'Inactivo'),(29,'Activo'),(30,'Inactivo'),(31,'Activo'),(32,'Inactivo'),(33,'Activo'),(34,'Inactivo'),(35,'Activo'),(36,'Inactivo'),(37,'Activo'),(38,'Inactivo'),(39,'Activo'),(40,'Inactivo'),(41,'Activo'),(42,'Inactivo'),(43,'Activo'),(44,'Inactivo'),(45,'Activo'),(46,'Inactivo'),(47,'Activo'),(48,'Inactivo'),(49,'Activo'),(50,'Inactivo');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturacompra`
--

DROP TABLE IF EXISTS `facturacompra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturacompra` (
  `IdFacturaVenta` bigint(20) NOT NULL AUTO_INCREMENT,
  `Valor` bigint(20) DEFAULT NULL,
  `Producto` varchar(100) DEFAULT NULL,
  `IdProveedor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdFacturaVenta`),
  KEY `ProveedorFactura` (`IdProveedor`),
  CONSTRAINT `facturacompra_ibfk_1` FOREIGN KEY (`IdProveedor`) REFERENCES `proveedor` (`IdProveedor`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturacompra`
--

LOCK TABLES `facturacompra` WRITE;
/*!40000 ALTER TABLE `facturacompra` DISABLE KEYS */;
INSERT INTO `facturacompra` VALUES (1,10000,'Cama',NULL),(2,20000,'Mesa',NULL),(3,30000,'closet',NULL),(4,40000,'Tocador',NULL),(5,50000,'Escritorio',NULL),(6,60000,'Silla',NULL),(7,70000,'Sofa',NULL),(8,80000,'Cajon',NULL),(9,90000,'Repisa',NULL),(10,100000,'ESTANTE',NULL),(11,10000,'Cama',NULL),(12,20000,'Mesa',NULL),(13,30000,'closet',NULL),(14,40000,'Tocador',NULL),(15,50000,'Escritorio',NULL),(16,60000,'Silla',NULL),(17,70000,'Sofa',NULL),(18,80000,'Cajon',NULL),(19,90000,'Repisa',NULL),(20,100000,'ESTANTE',NULL),(21,10000,'Cama',NULL),(22,20000,'Mesa',NULL),(23,30000,'closet',NULL),(24,40000,'Tocador',NULL),(25,50000,'Escritorio',NULL),(26,60000,'Silla',NULL),(27,70000,'Sofa',NULL),(28,80000,'Cajon',NULL),(29,90000,'Repisa',NULL),(30,100000,'ESTANTE',NULL),(31,10000,'Cama',NULL),(32,20000,'Mesa',NULL),(33,30000,'closet',NULL),(34,40000,'Tocador',NULL),(35,50000,'Escritorio',NULL),(36,60000,'Silla',NULL),(37,70000,'Sofa',NULL),(38,80000,'Cajon',NULL),(39,90000,'Repisa',NULL),(40,100000,'ESTANTE',NULL),(41,10000,'Cama',NULL),(42,20000,'Mesa',NULL),(43,30000,'closet',NULL),(44,40000,'Tocador',NULL),(45,50000,'Escritorio',NULL),(46,60000,'Silla',NULL),(47,70000,'Sofa',NULL),(48,80000,'Cajon',NULL),(49,90000,'Repisa',NULL),(50,100000,'ESTANTE',NULL);
/*!40000 ALTER TABLE `facturacompra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturaventa`
--

DROP TABLE IF EXISTS `facturaventa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturaventa` (
  `IdFacturaVenta` bigint(20) NOT NULL AUTO_INCREMENT,
  `producto` varchar(100) DEFAULT NULL,
  `NombreCliente` varchar(100) DEFAULT NULL,
  `IdCliente` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdFacturaVenta`),
  KEY `FacturaCliente` (`IdCliente`),
  CONSTRAINT `facturaventa_ibfk_1` FOREIGN KEY (`IdCliente`) REFERENCES `cliente` (`IdCliente`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturaventa`
--

LOCK TABLES `facturaventa` WRITE;
/*!40000 ALTER TABLE `facturaventa` DISABLE KEYS */;
INSERT INTO `facturaventa` VALUES (1,'Cama','Brayan Rojas',NULL),(2,'Mesa','Julian Maldonado',NULL),(3,'Silla','Jorge Pedraza',NULL),(4,'Tocador','Luis Lopez',NULL),(5,'Repisa','Gladys Torres',NULL),(6,'Estante','Sebastian Tenjo',NULL),(7,'Cajon','Lizeth Pardo',NULL),(8,'mueble','Brayan Olivares',NULL),(9,'sofa','David Ruiz',NULL),(10,'closet','Yeison Trujillo',NULL),(11,'Cama','Brayan Rojas',NULL),(12,'Mesa','Julian Maldonado',NULL),(13,'Silla','Jorge Pedraza',NULL),(14,'Tocador','Luis Lopez',NULL),(15,'Repisa','Gladys Torres',NULL),(16,'Estante','Sebastian Tenjo',NULL),(17,'Cajon','Lizeth Pardo',NULL),(18,'mueble','Brayan Olivares',NULL),(19,'sofa','David Ruiz',NULL),(20,'closet','Yeison Trujillo',NULL),(21,'Cama','Brayan Rojas',NULL),(22,'Mesa','Julian Maldonado',NULL),(23,'Silla','Jorge Pedraza',NULL),(24,'Tocador','Luis Lopez',NULL),(25,'Repisa','Gladys Torres',NULL),(26,'Estante','Sebastian Tenjo',NULL),(27,'Cajon','Lizeth Pardo',NULL),(28,'mueble','Brayan Olivares',NULL),(29,'sofa','David Ruiz',NULL),(30,'closet','Yeison Trujillo',NULL),(31,'Cama','Brayan Rojas',NULL),(32,'Mesa','Julian Maldonado',NULL),(33,'Silla','Jorge Pedraza',NULL),(34,'Tocador','Luis Lopez',NULL),(35,'Repisa','Gladys Torres',NULL),(36,'Estante','Sebastian Tenjo',NULL),(37,'Cajon','Lizeth Pardo',NULL),(38,'mueble','Brayan Olivares',NULL),(39,'sofa','David Ruiz',NULL),(40,'closet','Yeison Trujillo',NULL),(41,'Cama','Brayan Rojas',NULL),(42,'Mesa','Julian Maldonado',NULL),(43,'Silla','Jorge Pedraza',NULL),(44,'Tocador','Luis Lopez',NULL),(45,'Repisa','Gladys Torres',NULL),(46,'Estante','Sebastian Tenjo',NULL),(47,'Cajon','Lizeth Pardo',NULL),(48,'mueble','Brayan Olivares',NULL),(49,'sofa','David Ruiz',NULL),(50,'closet','Yeison Trujillo',NULL);
/*!40000 ALTER TABLE `facturaventa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventarioproducto`
--

DROP TABLE IF EXISTS `inventarioproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventarioproducto` (
  `IdInventarioProducto` bigint(20) NOT NULL AUTO_INCREMENT,
  `Cantidad` bigint(20) DEFAULT NULL,
  `TipoProducto` varchar(100) DEFAULT NULL,
  `NombreProducto` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IdInventarioProducto`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventarioproducto`
--

LOCK TABLES `inventarioproducto` WRITE;
/*!40000 ALTER TABLE `inventarioproducto` DISABLE KEYS */;
INSERT INTO `inventarioproducto` VALUES (1,100,'Cama','Cama'),(2,200,'Mesa','Mesa'),(3,300,'Silla','Silla'),(4,400,'Tocador','Tocador'),(5,500,'Sofa','Sofa'),(6,600,'Escritorio','Escritorio'),(7,700,'closet','Closet'),(8,800,'Cajon','Cajon'),(9,900,'estante','Estante'),(10,1000,'Repisa','Repisa'),(11,100,'Cama','Cama'),(12,200,'Mesa','Mesa'),(13,300,'Silla','Silla'),(14,400,'Tocador','Tocador'),(15,500,'Sofa','Sofa'),(16,600,'Escritorio','Escritorio'),(17,700,'closet','Closet'),(18,800,'Cajon','Cajon'),(19,900,'estante','Estante'),(20,1000,'Repisa','Repisa'),(21,100,'Cama','Cama'),(22,200,'Mesa','Mesa'),(23,300,'Silla','Silla'),(24,400,'Tocador','Tocador'),(25,500,'Sofa','Sofa'),(26,600,'Escritorio','Escritorio'),(27,700,'closet','Closet'),(28,800,'Cajon','Cajon'),(29,900,'estante','Estante'),(30,1000,'Repisa','Repisa'),(31,100,'Cama','Cama'),(32,200,'Mesa','Mesa'),(33,300,'Silla','Silla'),(34,400,'Tocador','Tocador'),(35,500,'Sofa','Sofa'),(36,600,'Escritorio','Escritorio'),(37,700,'closet','Closet'),(38,800,'Cajon','Cajon'),(39,900,'estante','Estante'),(40,1000,'Repisa','Repisa'),(41,100,'Cama','Cama'),(42,200,'Mesa','Mesa'),(43,300,'Silla','Silla'),(44,400,'Tocador','Tocador'),(45,500,'Sofa','Sofa'),(46,600,'Escritorio','Escritorio'),(47,700,'closet','Closet'),(48,800,'Cajon','Cajon'),(49,900,'estante','Estante'),(50,1000,'Repisa','Repisa');
/*!40000 ALTER TABLE `inventarioproducto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `IdProducto` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `IdInventarioProducto` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdProducto`),
  KEY `ProductoInventario` (`IdInventarioProducto`),
  CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`IdInventarioProducto`) REFERENCES `inventarioproducto` (`IdInventarioProducto`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (1,'Cama',NULL),(2,'Mesa',NULL),(3,'Silla',NULL),(4,'Tocador',NULL),(5,'Sofa',NULL),(6,'Escrtorio',NULL),(7,'Repisa',NULL),(8,'Estante',NULL),(9,'Cajon',NULL),(10,'Closet',NULL),(11,'Cama',NULL),(12,'Mesa',NULL),(13,'Silla',NULL),(14,'Tocador',NULL),(15,'Sofa',NULL),(16,'Escrtorio',NULL),(17,'Repisa',NULL),(18,'Estante',NULL),(19,'Cajon',NULL),(20,'Closet',NULL),(21,'Cama',NULL),(22,'Mesa',NULL),(23,'Silla',NULL),(24,'Tocador',NULL),(25,'Sofa',NULL),(26,'Escrtorio',NULL),(27,'Repisa',NULL),(28,'Estante',NULL),(29,'Cajon',NULL),(30,'Closet',NULL),(31,'Cama',NULL),(32,'Mesa',NULL),(33,'Silla',NULL),(34,'Tocador',NULL),(35,'Sofa',NULL),(36,'Escrtorio',NULL),(37,'Repisa',NULL),(38,'Estante',NULL),(39,'Cajon',NULL),(40,'Closet',NULL),(41,'Cama',NULL),(42,'Mesa',NULL),(43,'Silla',NULL),(44,'Tocador',NULL),(45,'Sofa',NULL),(46,'Escrtorio',NULL),(47,'Repisa',NULL),(48,'Estante',NULL),(49,'Cajon',NULL),(50,'Closet',NULL);
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `IdProveedor` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(100) DEFAULT NULL,
  `Direccion` varchar(100) DEFAULT NULL,
  `TelefonoCelular` bigint(20) DEFAULT NULL,
  `TelefonoFijo` bigint(20) DEFAULT NULL,
  `Correo` varchar(100) DEFAULT NULL,
  `IdEstado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdProveedor`),
  KEY `ProveedorEstado` (`IdEstado`),
  CONSTRAINT `proveedor_ibfk_1` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT INTO `proveedor` VALUES (1,'Brayan Rojas','av 45 n°65-78',111222333,12345,'Brayanrojas@gmail.com',NULL),(2,'Gladys Torres','av 12 n°78-78',444555666,67890,'GladysTorres@gmail.com',NULL),(3,'Anna Quiñones','av 23 n°12-78',777888999,12345,'AnnaQuiñones@gmail.com',NULL),(4,'Brayan Olivares','av 45 n°34-78',444555666,12345,'BrayanOlivares@gmail.com',NULL),(5,'Paola Cardenas','av 56 n°45-78',777888999,67890,'PaolCardenas@gmail.com',NULL),(6,'Sara Bernal','av 67 n°56-78',111222333,12345,'SaraBernal@gmail.com',NULL),(7,'Dayana Correa','av 78 n°67-78',444555666,67890,'DayanaCorrea@gmail.com',NULL),(8,'Jhonana Quintero','av 12 n°78-78',777888999,12345,'JhoannaQuintero@gmail.com',NULL),(9,'Hector Torres','av 23 n°65-87',111222333,67890,'HectorTorres@gmail.com',NULL),(10,'Brayan Rojas','av 45 n°67-78',111222333,12345,'Brayanrojas@gmail.com',NULL),(11,'Gladys Torres','av 12 n°78-78',444555666,67890,'GladysTorres@gmail.com',NULL),(12,'Anna Quiñones','av 23 n°12-78',777888999,12345,'AnnaQuiñones@gmail.com',NULL),(13,'Brayan Olivares','av 45 n°34-78',444555666,12345,'BrayanOlivares@gmail.com',NULL),(14,'Paola Cardenas','av 56 n°45-78',777888999,67890,'PaolCardenas@gmail.com',NULL),(15,'Sara Bernal','av 67 n°56-78',111222333,12345,'SaraBernal@gmail.com',NULL),(16,'Dayana Correa','av 78 n°67-78',444555666,67890,'DayanaCorrea@gmail.com',NULL),(17,'Jhonana Quintero','av 12 n°78-78',777888999,12345,'JhoannaQuintero@gmail.com',NULL),(18,'Hector Torres','av 23 n°65-87',111222333,67890,'HectorTorres@gmail.com',NULL),(19,'Brayan Rojas','av 45 n°67-78',111222333,12345,'Brayanrojas@gmail.com',NULL),(20,'Gladys Torres','av 12 n°78-78',444555666,67890,'GladysTorres@gmail.com',NULL),(21,'Anna Quiñones','av 23 n°12-78',777888999,12345,'AnnaQuiñones@gmail.com',NULL),(22,'Brayan Olivares','av 45 n°34-78',444555666,12345,'BrayanOlivares@gmail.com',NULL),(23,'Paola Cardenas','av 56 n°45-78',777888999,67890,'PaolCardenas@gmail.com',NULL),(24,'Sara Bernal','av 67 n°56-78',111222333,12345,'SaraBernal@gmail.com',NULL),(25,'Dayana Correa','av 78 n°67-78',444555666,67890,'DayanaCorrea@gmail.com',NULL),(26,'Jhonana Quintero','av 12 n°78-78',777888999,12345,'JhoannaQuintero@gmail.com',NULL),(27,'Hector Torres','av 23 n°65-87',111222333,67890,'HectorTorres@gmail.com',NULL),(28,'Brayan Rojas','av 45 n°67-78',111222333,12345,'Brayanrojas@gmail.com',NULL),(29,'Gladys Torres','av 12 n°78-78',444555666,67890,'GladysTorres@gmail.com',NULL),(30,'Anna Quiñones','av 23 n°12-78',777888999,12345,'AnnaQuiñones@gmail.com',NULL),(31,'Brayan Olivares','av 45 n°34-78',444555666,12345,'BrayanOlivares@gmail.com',NULL),(32,'Paola Cardenas','av 56 n°45-78',777888999,67890,'PaolCardenas@gmail.com',NULL),(33,'Sara Bernal','av 67 n°56-78',111222333,12345,'SaraBernal@gmail.com',NULL),(34,'Dayana Correa','av 78 n°67-78',444555666,67890,'DayanaCorrea@gmail.com',NULL),(35,'Jhonana Quintero','av 12 n°78-78',777888999,12345,'JhoannaQuintero@gmail.com',NULL),(36,'Hector Torres','av 23 n°65-87',111222333,67890,'HectorTorres@gmail.com',NULL),(37,'Brayan Rojas','av 45 n°67-78',111222333,12345,'Brayanrojas@gmail.com',NULL),(38,'Gladys Torres','av 12 n°78-78',444555666,67890,'GladysTorres@gmail.com',NULL),(39,'Anna Quiñones','av 23 n°12-78',777888999,12345,'AnnaQuiñones@gmail.com',NULL),(40,'Brayan Olivares','av 45 n°34-78',444555666,12345,'BrayanOlivares@gmail.com',NULL),(41,'Paola Cardenas','av 56 n°45-78',777888999,67890,'PaolCardenas@gmail.com',NULL),(42,'Sara Bernal','av 67 n°56-78',111222333,12345,'SaraBernal@gmail.com',NULL),(43,'Dayana Correa','av 78 n°67-78',444555666,67890,'DayanaCorrea@gmail.com',NULL),(44,'Jhonana Quintero','av 12 n°78-78',777888999,12345,'JhoannaQuintero@gmail.com',NULL),(45,'Hector Torres','av 23 n°65-87',111222333,67890,'HectorTorres@gmail.com',NULL),(46,'Brayan Olivares','av 45 n°34-78',444555666,12345,'BrayanOlivares@gmail.com',NULL),(47,'Paola Cardenas','av 56 n°45-78',777888999,67890,'PaolCardenas@gmail.com',NULL),(48,'Sara Bernal','av 67 n°56-78',111222333,12345,'SaraBernal@gmail.com',NULL),(49,'Dayana Correa','av 78 n°67-78',444555666,67890,'DayanaCorrea@gmail.com',NULL),(50,'Gladys Torres','av 12 n°78-78',444555666,67890,'GladysTorres@gmail.com',NULL);
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo`
--

DROP TABLE IF EXISTS `tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo` (
  `IdTipo` bigint(20) NOT NULL AUTO_INCREMENT,
  `Cantidad` bigint(20) DEFAULT NULL,
  `FechaE` date DEFAULT NULL,
  `IdProducto` bigint(20) DEFAULT NULL,
  `IdProveedor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdTipo`),
  KEY `ProductoTipo` (`IdProducto`),
  KEY `ProveedorTipo` (`IdProveedor`),
  CONSTRAINT `tipo_ibfk_2` FOREIGN KEY (`IdProveedor`) REFERENCES `proveedor` (`IdProveedor`),
  CONSTRAINT `tipo_ibfk_1` FOREIGN KEY (`IdProducto`) REFERENCES `producto` (`IdProducto`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo`
--

LOCK TABLES `tipo` WRITE;
/*!40000 ALTER TABLE `tipo` DISABLE KEYS */;
INSERT INTO `tipo` VALUES (1,100,'2016-04-12',NULL,NULL),(2,200,'2015-06-12',NULL,NULL),(3,300,'2013-07-12',NULL,NULL),(4,400,'2012-08-12',NULL,NULL),(5,500,'2011-09-12',NULL,NULL),(6,600,'2010-10-12',NULL,NULL),(7,700,'0000-00-00',NULL,NULL),(8,800,'0000-00-00',NULL,NULL),(9,900,'0000-00-00',NULL,NULL),(10,1000,'0000-00-00',NULL,NULL),(11,100,'2016-04-12',NULL,NULL),(12,200,'2015-06-12',NULL,NULL),(13,300,'2013-07-12',NULL,NULL),(14,400,'2012-08-12',NULL,NULL),(15,500,'2011-09-12',NULL,NULL),(16,600,'2010-10-12',NULL,NULL),(17,700,'0000-00-00',NULL,NULL),(18,800,'0000-00-00',NULL,NULL),(19,900,'0000-00-00',NULL,NULL),(20,1000,'0000-00-00',NULL,NULL),(21,100,'2016-04-12',NULL,NULL),(22,200,'2015-06-12',NULL,NULL),(23,300,'2013-07-12',NULL,NULL),(24,400,'2012-08-12',NULL,NULL),(25,500,'2011-09-12',NULL,NULL),(26,600,'2010-10-12',NULL,NULL),(27,700,'0000-00-00',NULL,NULL),(28,800,'0000-00-00',NULL,NULL),(29,900,'0000-00-00',NULL,NULL),(30,1000,'0000-00-00',NULL,NULL),(31,100,'2016-04-12',NULL,NULL),(32,200,'2015-06-12',NULL,NULL),(33,300,'2013-07-12',NULL,NULL),(34,400,'2012-08-12',NULL,NULL),(35,500,'2011-09-12',NULL,NULL),(36,600,'2010-10-12',NULL,NULL),(37,700,'0000-00-00',NULL,NULL),(38,800,'0000-00-00',NULL,NULL),(39,900,'0000-00-00',NULL,NULL),(40,1000,'0000-00-00',NULL,NULL),(41,100,'2016-04-12',NULL,NULL),(42,200,'2015-06-12',NULL,NULL),(43,300,'2013-07-12',NULL,NULL),(44,400,'2012-08-12',NULL,NULL),(45,500,'2011-09-12',NULL,NULL),(46,600,'2010-10-12',NULL,NULL),(47,700,'0000-00-00',NULL,NULL),(48,800,'0000-00-00',NULL,NULL),(49,900,'0000-00-00',NULL,NULL),(50,1000,'0000-00-00',NULL,NULL);
/*!40000 ALTER TABLE `tipo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-18 12:30:53
